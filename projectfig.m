function varargout = projectfig(varargin)
% PROJECTFIG MATLAB code for projectfig.fig
%      PROJECTFIG, by itself, creates a new PROJECTFIG or raises the existing
%      singleton*.
%
%      H = PROJECTFIG returns the handle to a new PROJECTFIG or the handle to
%      the existing singleton*.
%
%      PROJECTFIG('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJECTFIG.M with the given input arguments.
%
%      PROJECTFIG('Property','Value',...) creates a new PROJECTFIG or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before projectfig_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to projectfig_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help projectfig

% Last Modified by GUIDE v2.5 23-Feb-2018 22:04:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @projectfig_OpeningFcn, ...
                   'gui_OutputFcn',  @projectfig_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before projectfig is made visible.
function projectfig_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to projectfig (see VARARGIN)

% Choose default command line output for projectfig
handles.output = hObject;

% Setting values in list
audiosListItem = getAudios();
handles.audiosListItems = getAudios();
set(handles.audiosList, 'String', audiosListItem);

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using projectfig.
if strcmp(get(hObject,'Visible'),'off')
    plot(rand(5));
end

% UIWAIT makes projectfig wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = projectfig_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in fftTempGenerateOne.
function fftTempGenerateOne_Callback(hObject, eventdata, handles)
% hObject    handle to fftTempGenerateOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%axes(handles.graphic2);
%cla;

%popup_sel_index = get(handles.popupmenu1, 'Value');
%switch popup_sel_index
%    case 1
%        plot(rand(5));
%    case 2
%        plot(sin(1:0.01:25.99));
%%    case 3
%        bar(1:.5:10);
%    case 4
%        plot(membrane);
%    case 5
%        surf(peaks);
%end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)

% --- Executes on button press in fftTempGenerateAll.
function fftTempGenerateAll_Callback(hObject, eventdata, handles)
% hObject    handle to fftTempGenerateAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in loadFilter.
function loadFilter_Callback(hObject, eventdata, handles)
% hObject    handle to loadFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in tempGenerateOne.
function tempGenerateOne_Callback(hObject, eventdata, handles)
% hObject    handle to tempGenerateOne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = clock;
folderRootName = strcat(int2str(data(1)),int2str(data(2)),int2str(data(3)),int2str(data(4)),int2str(data(5)),int2str(data(6)));
mkdir(strcat('output/', folderRootName));
generateOneTemplate(hObject, handles, folderRootName);


% --- Executes on button press in tempGenerateAll.
function tempGenerateAll_Callback(hObject, eventdata, handles)
% hObject    handle to tempGenerateAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = clock;
folderRootName = strcat(int2str(data(1)),int2str(data(2)),int2str(data(3)),int2str(data(4)),int2str(data(5)),int2str(data(6)));
mkdir(strcat('output/', folderRootName));
generateAllTemplates(hObject, handles, folderRootName);


function templateSizeET_Callback(hObject, eventdata, handles)
% hObject    handle to templateSizeET (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of templateSizeET as text
%        str2double(get(hObject,'String')) returns contents of templateSizeET as a double
try
    if isempty(eventdata.Source.String)
        return;
    end

    windowSize = str2num(eventdata.Source.String);

    if isnumeric(windowSize) && mean(windowSize) > 0 && mod(windowSize, 1) == 0
        adjustWindow(windowSize, hObject, handles);
    else
        alert('message', 'VALOR INV�LIDO! UTILIZE SOMENTE INTEIROS.', 'title', 'ERRO!');
    end
catch
    alert('message', 'VALOR INV�LIDO! UTILIZE SOMENTE INTEIROS.', 'title', 'ERRO! -TC');
end


% --- Executes during object creation, after setting all properties.
function templateSizeET_CreateFcn(hObject, eventdata, handles)
% hObject    handle to templateSizeET (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in audiosList.
function audiosList_Callback(hObject, eventdata, handles)
% hObject    handle to audiosList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns audiosList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from audiosList

%% This code detects if the click was single or double and open the audio in case of double click
persistent audioListChk
persistent audioListValueChk
if isempty(audioListChk)
      audioListChk = 1;
      audioListValueChk = eventdata.Source.Value;
      pause(0.5); % Add a delay to distinguish single click from a double click
      if audioListChk == 1
          %fprintf(1,'\nI am doing a single-click.\n\n');
          audioListChk = [];
          audioListValueChk = [];
      end
else
      if audioListValueChk == eventdata.Source.Value
          clickedItem = handles.audiosListItems(eventdata.Source.Value);
          clickedItem = clickedItem{1};
          setAudioGraphics(clickedItem, hObject, handles);
          %fprintf(1,'\nI am doing a double-click in.\n\n');
      end
      audioListChk = [];
      audioListValueChk = [];
end


% --- Executes during object creation, after setting all properties.
function audiosList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to audiosList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function setAudioGraphics(itemName, hObject, handles)
    [audioFile, Fs, audioInfo] = getAudio(itemName);
    audioFileLength = size(audioFile, 1);
    T = 1/Fs;
    n = 0:T:(audioFileLength/Fs)-T;
    
    handles.selectedAudio.title = itemName;
    handles.selectedAudio.data = audioFile;
    handles.selectedAudio.info = audioInfo;
    handles.selectedAudio.Fs = Fs;
    guidata(hObject, handles);
    
    handles.selectedAudioTitle.String = itemName;
    
    axes(handles.graphic1);
    cla;
    plot(n, audioFile(:,1));
    axes(handles.graphic2);
    cla;
    if size(audioFile, 2) > 1
        plot(n, audioFile(:,2));
        handles.graphic2.Visible = 'on';
        handles.graphic1.YLim = handles.graphic2.YLim;
    else
        handles.graphic2.Visible = 'off';
    end

function adjustWindow(windowSize, hObject, handles)
    if ~isfield(handles, 'selectedAudio')
        alert('message', 'Selecione um �udio, por favor.', 'title', 'ERRO!');
        return
    end
    try
        path = strcat('tracks/', handles.selectedAudio.title, '.tracks');
        file = fileread(path);
    catch
        alert('message', strcat('Este �udio n�o possui marca��o. Crie o arquivo: ../', path), 'title', 'ERRO!');
        return
    end
    tracks = strsplit(file, '\n');
    windowCenter = str2num(tracks{1});
    windowSize = windowSize - mod(windowSize, 2);
    XLim = [windowCenter-(windowSize/2) windowCenter+(windowSize/2)];
    XLim = XLim/handles.selectedAudio.Fs;
    
    handles.windowSetting.size = windowSize;
    guidata(hObject, handles);
    
    axes(handles.graphic1);
    handles.graphic1.XLim = XLim;
    axes(handles.graphic2);
    handles.graphic2.XLim = XLim;

function generateOneTemplate(hObject, handles, folderRootName)
    if ~isfield(handles, 'selectedAudio')
        alert('message', 'Selecione um �udio, por favor.', 'title', 'ERRO!');
        return
    end
    if ~(isfield(handles, 'windowSetting') && isfield(handles.windowSetting, 'size'))
        alert('message', 'Digite o tamanho do template, por favor.', 'title', 'ERRO!');
        return
    end
    try
        path = strcat('tracks/', handles.selectedAudio.title, '.tracks');
        file = fileread(path);
    catch
        alert('message', strcat('Este �udio n�o possui marca��o. Crie o arquivo: ../', path), 'title', 'ERRO!');
        return
    end
    selectedAudio = handles.selectedAudio;
    
    % criar e colocar nome da pasta
    folderName = selectedAudio.title;
    if(size(folderName,2) > 4)
        folderName = folderName(1:size(folderName,2)-4);
    end
    mkdir(strcat('output/', folderRootName, '/', folderName));
    path = strcat('output/', folderRootName, '/', folderName);
    n = 1;
    
    windowSize = handles.windowSetting.size;
    windowSize = windowSize - mod(windowSize, 2);
    
    tracks = strsplit(file, '\n');
    
    windowLastStart = 0;
    windowLastEnd = 0;
    canGenerate = 1;
    for i=1:size(tracks,2)
        if(strcmp(tracks{i},''))
            break;
        end
        windowCenter = str2num(tracks{i});
        windowStart = windowCenter-(windowSize/2);
        
        if(windowLastEnd ~= 0 && windowStart > windowLastEnd && canGenerate)
            generateTemplate(selectedAudio, windowLastStart, windowLastEnd, path, n);
            n = n+1;
        elseif(windowStart < windowLastEnd)
            canGenerate = 0;
        else
            canGenerate = 1;
        end
        windowLastStart = windowStart;
        windowLastEnd = windowCenter+(windowSize/2);
    end
    
    if(windowLastEnd ~= 0 && canGenerate)
        generateTemplate(selectedAudio, windowLastStart, windowLastEnd, path, n);
    end
    
function generateAllTemplates(hObject, handles, folderRootName)
    if ~(isfield(handles, 'windowSetting') && isfield(handles.windowSetting, 'size'))
        alert('message', 'Digite o tamanho do template, por favor.', 'title', 'ERRO!');
        return
    end
    
    handles.selectedAudioTitle.String = 'Working...';
    
    windowSize = handles.windowSetting.size;
    windowSize = windowSize - mod(windowSize, 2);
    
    audiosList = handles.audiosListItems;
    
    for i=1:length(audiosList)
        audioItemName = audiosList(i);
        audioItemName = audioItemName{1};
        
        handles.selectedAudioTitle.String = strcat('Working... ', audioItemName);
        
        [audioFile, Fs, audioInfo] = getAudio(audioItemName);
        selectedAudio.title = audioItemName;
        selectedAudio.data = audioFile;
        selectedAudio.info = audioInfo;
        selectedAudio.Fs = Fs;
        
        isOK = 1;
        try
            path = strcat('tracks/', selectedAudio.title, '.tracks');
            file = fileread(path);
        catch
            isOK = 0;
        end
        
        if(isOK)
            % criar e colocar nome da pasta
            folderName = selectedAudio.title;
            if(size(folderName,2) > 4)
                folderName = folderName(1:size(folderName,2)-4);
            end
            mkdir(strcat('output/', folderRootName, '/', folderName));
            path = strcat('output/', folderRootName, '/', folderName);
            n = 1;

            tracks = strsplit(file, '\n');

            windowLastStart = 0;
            windowLastEnd = 0;
            canGenerate = 1;
            for i=1:size(tracks,2)
                if(strcmp(tracks{i},''))
                    break;
                end
                windowCenter = str2num(tracks{i});
                windowStart = windowCenter-(windowSize/2);

                if(windowLastEnd ~= 0 && windowStart > windowLastEnd && canGenerate)
                    generateTemplate(selectedAudio, windowLastStart, windowLastEnd, path, n);
                    n = n+1;
                elseif(windowStart < windowLastEnd)
                    canGenerate = 0;
                else
                    canGenerate = 1;
                end
                windowLastStart = windowStart;
                windowLastEnd = windowCenter+(windowSize/2);
            end

            if(windowLastEnd ~= 0 && canGenerate)
                generateTemplate(selectedAudio, windowLastStart, windowLastEnd, path, n);
            end
        end
    end
    handles.selectedAudioTitle.String = 'OK!';
    
function generateTemplate(selectedAudio, windowStart, windowEnd, folderName, n)
    newTemplate = zeros(windowEnd-windowStart+1, selectedAudio.info.NumChannels);
    for j=1:size(newTemplate, 2)
        for i=1:size(newTemplate, 1)
            newTemplate(i, j) = selectedAudio.data(windowStart + i - 1);
        end
    end
    Fs = selectedAudio.Fs;
    newFileName = strcat(folderName, '/', selectedAudio.title, int2str(n), '.m4a');
    
    audiowrite(newFileName, newTemplate, Fs);