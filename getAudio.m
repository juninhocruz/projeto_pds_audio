function [audiofile, Fs, audioInfo] = getAudio(filename)
    path = strcat('todos/', filename);
    [audiofile, Fs] = audioread(path);
    audioInfo = audioinfo(path);
end